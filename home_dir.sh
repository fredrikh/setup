#! /bin/bash

###############################################################################
# home_dir.sh
#
# Removes all unnessecary directories from ~, renames
# Downloads to downloads and Desktop to .desktop
###############################################################################

script_dir=$(dirname $(readlink -f $0))
$script_dir/safe_copy.sh $script_dir/user-dirs.dirs $HOME/.config/user-dirs.dirs 

mv $HOME/Downloads $HOME/downloads
mv $HOME/Desktop $HOME/.desktop

for file in Documents Music Pictures Public Videos Templates; do
    echo "Deleting $HOME/$file"
    rm -rf $HOME/$file
done
