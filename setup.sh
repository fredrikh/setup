#! /bin/bash

###############################################################################
# setup.sh
#
# Runs all setup scripts
###############################################################################

script_dir=$(dirname $(readlink -f $0))

$script_dir/home_dir.sh
$script_dir/programs.sh
$script_dir/python.sh
