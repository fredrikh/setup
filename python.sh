#!/bin/bash

###############################################################################
# python.sh
#
# Sets up virtualenvwrapper to work with venv
###############################################################################

sudo apt install python3-pip python3-venv python3-virtualenvwrapper

mkdir -p $HOME/.venvs $HOME/code

# Add venv environment variables
sed -i "/^# Which plugins would you like to load?/i \
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3\n\
export VIRTUALENVWRAPPER_VIRTUALENV=python3\n\
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='-m venv'\n\
export WORKON_HOME=$HOME/.venvs\n\
export PROJECT_HOME=$HOME/code\n" $HOME/.zshrc

# Add virtualenvwrapper plugin
sed -i "s/\(^plugins=.*\))$/\1 virtualenvwrapper)/" $HOME/.zshrc
