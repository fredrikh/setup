#!/bin/bash

###############################################################################
# safe_copy.sh
#
# Creates symlinks from the first argument to the second,
# storing a copy of the old file if it exists
###############################################################################

backup_dir=backup  # old dotfiles backup directory

copy_from=$1       # file name to copy from (e.g. vimrc)
copy_to=$2         # file_name to write file to (e.g. $HOME/.vimrc)

# Store old version if it exists
if [ -f "$copy_to" ]; then
    mkdir -p $backup_dir
    mv $copy_to $backup_dir
fi

echo "Creating symlink from  $copy_from to $copy_to"
ln -s $copy_from $copy_to

# Remove bakcup_dir if its empty
if [ -d "$backup_dir" ]; then
    if [ -z "$(ls -A $backup_dir)" ]; then
        rm -r $backup_dir
    fi
fi

