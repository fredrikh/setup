dotfiles
========

Scripts used to set up a new machine. Intended to be cloned as a hidden folder
in the home dir. The script `setup.sh` installs everything.

