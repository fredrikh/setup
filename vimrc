"" Must be first
set nocompatible
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"let path = '~/some/path/here'
"call vundle#rc(path)

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between here and filetype plugin indent on.
" scripts on GitHub repos
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
" Plugin 'xolox/vim-easytags'
" Plugin 'xolox/vim-misc' " needed by easy-tags
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'tyru/DumbBuf.vim'
" Plugin 'ycm-core/YouCompleteMe'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-scripts/indentpython.vim'
"Plugin 'Lokaltog/vim-easymotion'
"Plugin 'tpope/vim-rails.git'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" scripts from http://vim-scripts.org/vim/scripts.html
" Plugin 'taglist.vim'
Plugin 'undotree.vim'
Plugin 'YankRing.vim'
"Plugin 'gnu-c'
" scripts not on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" ...

"all of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on     " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList          - list configured plugins
" :PluginInstall(!)    - install (update) plugins
" :PluginSearch(!) foo - search (or refresh cache first) for foo
" :PluginClean(!)      - confirm (or auto-approve) removal of unused plugins
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Plugin commands are not allowed.
" Put your stuff after this line

"" In Makefiles DO NOT use spaces instead of tabs
autocmd FileType make setlocal noexpandtab

set number " Show line numbers
set cursorline " Highlight current line
set autoindent " Better indentation
set showmode " Show VISUAL and INSERT mode at bottom
set showcmd " Show command being input (like cw)
set hidden " Hide abandoned files instead of unloading
set wildmenu " Shows autocomplete alternatives
set wildmode=list:longest " Complete until first different character
set visualbell " No sound alarm
set ttyfast " Smoother drawing for fast terminals
set ruler " Show line number and column in bottom line
set backspace=indent,eol,start " Make backspace behave normally
set laststatus=2 " Always show status line
set ignorecase
set smartcase " Works with the above to make search case smart
set hlsearch " Highlight searched phrases
set incsearch " Highlight while searching
set showmatch " Briefly jump to matching bracket
set gdefault " Make search global by default
set linebreak " wordwrap

set background=dark

set foldmethod=indent
set foldlevel=99

set clipboard=unnamedplus

"" Tab settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

"Remove search highlight
nnoremap <leader><space> :noh<cr>

"Mark text that was just pasted
nnoremap <leader>v V`]

"Open new vertical window and switch to it
nnoremap <leader>w <C-w>v<C-w>l

"Easier window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


"Make up and down work for real lines
nnoremap j gj
nnoremap k gk

syntax on
set modelines=0

" Open Yankring-menu
nnoremap <silent> <F3> :YRShow<cr>
inoremap <silent> <F3> <ESC>:YRShow<cr>

" Set DumbBuf key
let dumbbuf_hotkey = '<F4>'

" Open NERDTree
nnoremap <silent> <F7> :NERDTreeToggle<cr>
inoremap <silent> <F7> <ESC>:NERDTreeToggle<cr>

" Open tag list
nnoremap <silent> <F2> :TlistToggle<cr><C-w>h
inoremap <silent> <F2> <ESC>:TlistToggle<cr><C-w>h

let no_flake8_maps = 1
autocmd FileType python map <buffer> <F8> :call flake8#Flake8()<CR>

nnoremap <silent> <F9> :TagbarToggle<cr><C-w>l
inoremap <silent> <F9> <ESC>:TagbarToggle<cr><C-w>l
"nmap <F9> :TagbarToggle<CR>

" Open undo tree
nnoremap <F5> :UndotreeToggle<CR><cr><C-w>h

" Trigger configuration. Do not use <tab> if you use YouCompleteMe
let g:UltiSnipsExpandTrigger="<C-space>"
nnoremap <leader>u :call UltiSnips#ListSnippets() <cr>

" Ultisnips usage
" ctrl + space : paste snip
" ctrl + j     : jump to next placeholder
" ctrl + k     : jump to previous placeholder

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetDir="~/.vim/bundle/vim-snippets/UltiSnips/"

let g:tex_flavor="tex"

let g:yankring_history_dir="~/.vim/"

" Tag command
nnoremap æ <C-]>
nnoremap ø <C-t>

set tags=./tags;$HOME
let g:easytags_dynamic_files = 1
"set tags+=~/tagtest/lib/tags

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
map <leader>f  :YcmCompleter FixIt<CR>

"" Python syntax highlighting
autocmd BufRead,BufNewFile *.py let python_highlight_all=1
