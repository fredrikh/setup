#!/bin/bash

###############################################################################
# programs.sh
#
# Installs and sets up:
#   vim
#   zsh
#   tmux
###############################################################################

sudo apt install -y vim-nox zsh tmux # exuberant-ctags

script_dir=$(dirname $(readlink -f $0))

# Copy vimrc
$script_dir/safe_copy.sh $script_dir/vimrc $HOME/.vimrc
$script_dir/safe_copy.sh $script_dir/tmux.conf $HOME/.tmux.conf
$script_dir/safe_copy.sh $script_dir/flake8 $HOME/.config/flake8

# Clone Vundle into vim dir and install bundles
mkdir -p ~/.vim/bundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

# Install Oh My Zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Set zsh theme to muse
sed -i 's/ZSH_THEME=".*"/ZSH_THEME="muse"/' $HOME/.zshrc 

# Set editor to vim
echo "export EDITOR='vim'" >> $HOME/.zshrc
